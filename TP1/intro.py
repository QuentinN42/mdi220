import numpy as np
from numba import vectorize, njit
from string import ascii_lowercase


@vectorize
def nextpower(x):
    curr_pow = np.log2(x)
    curr_pow_top = np.ceil(curr_pow)
    return 2**curr_pow_top


def q2():
    return ascii_lowercase[2::3]


def q3():
    return np.round(np.pi, 9)


def char_freq(word: str) -> dict:
    return {
        c: word.count(c)
        for c in set(word)
    }


def cesar(word: str, shift: int) -> str:
    return "".join(chr((ord(c) + shift) % 128) for c in word)


@njit
def q6(n):
    x = 4*np.arange(1, n)**2
    return 2*(x/(x-1)).prod()


def quicksort(lst: list) -> list:
    if len(lst) <= 1:
        return lst
    else:
        pivot = lst.pop(-1)
        less = []
        greater = []
        for x in lst:
            if x <= pivot:
                less.append(x)
            else:
                greater.append(x)
    return quicksort(less) + [pivot] + quicksort(greater)


def q8():
    mat = np.random.uniform(-1, 1, (5, 6))
    mat[::2] = mat[::2] - 2*np.concatenate([mat[1::2], np.zeros((1, 6))])
    return np.where(mat < 0, 0, mat)


def q9():
    m = np.random.uniform(-1, 1, (5, 20))
    return m @ m.T
