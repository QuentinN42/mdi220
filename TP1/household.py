import pandas as pd
pd.options.plotting.backend = "plotly"


df = pd.read_csv("household.csv", sep=";", index_col=1)


if __name__ == '__main__':
    print(*df.columns)
    print(df.shape)
    s = df.shape
    df.dropna(inplace=True)
    print(f"drop na : {s[0]-df.shape[0]} removed !")
    print(df.shape)
    df.set_index("Date")
    df.index = pd.DatetimeIndex(df.index)
    print(df)
    print(df.between_time("2007-1-1", "2007-4-30"))
