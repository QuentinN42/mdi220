import numpy as np
import pytest
from random import shuffle
from intro import nextpower, q2, q3, char_freq, cesar, q6, quicksort, q8, q9
from time import time

param = pytest.mark.parametrize


@param("inp, out", [
    (1, 1),
    (3, 4),
    (127, 128),
    (129, 256)
])
def test_nextpower(inp, out):
    assert nextpower(inp) == out


def test_q2():
    assert q2() == "cfilorux"


def test_q3():
    assert q3() == 3.141592654


@param("inp, out", [
    ("aaa", {"a": 3}),
    ("aba", {"a": 2, "b": 1}),
    ("HelLo WorLd!!!", {"H": 1, "e": 1, "l": 1, "L": 2, "o": 2, " ": 1, "W": 1, "r": 1, "d": 1, "!": 3})
])
def test_char_freq(inp, out):
    assert char_freq(inp) == out


@param("inp, shift, out", [
    ("abc", 0, "abc"),
    ("abc", 1, "bcd"),
    (chr(127), 1, chr(0))
])
def test_cesar(inp, shift, out):
    assert cesar(inp, shift) == out


@param("n, div, max_time", [
    (1, 2, 2),
    (1_000, 0.1, 1),
    (1_000_000, 0.01, 1),
])
def test_q6(n, div, max_time):
    t = time()
    r = q6(n)
    delta_t = time() - t
    assert delta_t < max_time
    assert np.pi - div < r < np.pi + div


s = list(range(100))
shuffle(s)


@param("inp, out", [
    ([], []),
    ([1, 2], [1, 2]),
    ([2, 1], [1, 2]),
    (s, list(range(100))),
])
def test_quicksort(inp, out):
    assert quicksort(inp) == out


def test_q8():
    assert q8().min() >= 0


def test_q9():
    mat = q9()
    assert np.all(mat == mat.T)
    assert np.all(np.linalg.eig(mat)[0] >= 0)
